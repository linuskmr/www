# www.kytta.dev

## Licence

Copyright © 2021–2022 [Nikita Karamov](https://www.kytta.dev/)  
Licenced under the [GNU Affero General Public License v3.0 only](https://spdx.org/licenses/AGPL-3.0-only.html).

---

This project is hosted on Codeberg: <https://codeberg.org/kytta/www>
